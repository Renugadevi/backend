const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const router = express.Router();
var jwt = require('jsonwebtoken');

var config = require('../config');
const { check, validationResult } = require('express-validator');
const isEmail = (email) => {
  if (typeof email !== 'string') {
    return false;
  }

  const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
  return emailRegex.test(email);
};
router.post('/signup',async(req, res) => {
  try {
    const {first_name,last_name,email,phone,password} = req.body;
    if (!isEmail(email)) {
      throw new Error('Email must be a valid email address.');
    }
    if (/^\d{10}$/.test(phone))
    {
      res.status(200);
    }
    else
    {
       throw new Error('invalid phone number');
    }
    const registereduser=await User.findOne({ email});
    if(registereduser)
    {
      throw new Error('User already exists');
    }
    const registerednumber=await User.findOne({ phone});
    if(registerednumber)
    {
      throw new Error('User already exists');
    }
    const user = new User({ first_name,last_name,email,phone,password});
    const persistedUser = await user.save();
    var token = jwt.sign({ id: user._id }, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });

    res.status(201).json({
      title: 'User Registration Successful',
      detail: 'Successfully registered new user',
      auth: true,token:token,
    });
  } catch (err) {
     res.status(400).json({
      errors: [
        {
          title: 'Registration Error',
          detail: 'Something went wrong during registration process.',
          errorMessage: err.message,
        },
      ],
    });
  }
});

router.post('/login/email', async (req, res) => {
    try {
      const { email, password } = req.body;
      if (!isEmail(email)) {
        return res.status(400).json({
          errors: [
            {
              title: 'Bad Request',
              detail: 'Email must be a valid email address',
            },
          ],
        });
      }
      if (typeof password !== 'string') {
        return res.status(400).json({
          errors: [
            {
              title: 'Bad Request',
              detail: 'Password must be a string',
            },
          ],
        });
      }
      //queries database to find a user with the received email
      const user = await User.findOne({ email });
      if (!user) {
        throw new Error("Invalid Email");
      }
      const passwordValidated = await bcrypt.compare(password, user.password);
      if (!passwordValidated) {
        throw new Error("Invalid Password");
      }
  
      res.json({
        title: 'Login Successful',
        detail: 'Successfully validated user credentials',
      });
    } catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct email or password.. If you dont have an account signup here',
            errorMessage: err.message,
          },
        ],
      });
    }
  });
  
  
router.post('/login/phone', async (req, res) => {

    try {
      const { phone, password } = req.body;
      //queries database to find a user with the received phone
      const user= await User.findOne({ phone });
      if (!user) {
        throw new Error("Invalid User/Phone number");
      }
      const passwordValidated = await bcrypt.compare(password, user.password);
      if (!passwordValidated) {
        throw new Error("Password mismatched");
      }
  
      res.json({
        title: 'Login Successful',
        detail: 'Successfully validated user credentials',
      });
    } catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct email or contact or password combination.. Dont have an account  plz signup',
            errorMessage: err.message,
          },
        ],
      });
    }
  });

router.post('/forgot', async (req, res) => {
    try {
      const { email } = req.body;
      if (!isEmail(email)) {
        return res.status(400).json({
          errors: [
            {
              title: 'Bad Request',
              detail: 'Email must be a valid email address',
            },
          ],
        });
      }

      //queries database to find a user with the received email
      const user = await User.findOne({ email });
      if (!user) {
        throw new Error("Invalid Email");
      }
      res.json({
        title: 'Send Reset Password link',
        detail: 'Successfully validated user credentials and sent reset password link',
      });
    } catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct email address..',
            errorMessage: err.message,
          },
        ],
      });
    }
  });


router.post('/forgot/phone', async (req, res) => {

    try {
      const { phone } = req.body;
      //queries database to find a user with the received phone
      const user= await User.findOne({ phone });
      if (!user) {
        throw new Error("Invalid Phone number");
      }
  
      res.json({
        title: 'Send Reset Password link',
        detail: 'Successfully validated user credentials and sent reset password link',
      });
    } catch (err) {
      res.status(401).json({
        errors: [
          {
            title: 'Invalid Credentials',
            detail: 'please enter correct phone number.. ',
            errorMessage: err.message,
          },
        ],
      });
    }
  });
  
    
router.post('/search', async (req, res) => {

      
    var query = { full_name: /^r/ };  
    db.collection("users").find(query).toArray(function(err, res) {  
    if (err) throw err;  
    console.log(res);    
});
  });
 
module.exports = router;

