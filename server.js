const { getSecret } = require('./secrets');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const usersRoute = require('./routes/users');
const AssetsRoute = require('./routes/asset');
const NomineesRoute = require('./routes/nominee');
const fileUploadRoute=require('./routes/file')

mongoose.Promise = global.Promise;
mongoose.connect( 'mongodb://localhost:27017/SaftyNet',
    { 
      useNewUrlParser: true,
      useUnifiedTopology: true  
    })
  .then(
    () => {
      console.log('Connected to mongoDB');
    },
    (err) => console.log('Error connecting to mongoDB', err)
  );
const app = express();
const port = process.env.PORT || 4000;
app.use(bodyParser.json());
app.use(cookieParser());
app.use('/api/users', usersRoute);
app.use('/api/assets', AssetsRoute);
app.use('/api/nominees', NomineesRoute);
const file = require('./routes/file');
app.use('/uploads', express.static('uploads'));
app.use('/api/fileUpload', file);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
module.exports = { app }

