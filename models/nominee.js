const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');

const NomineeSchema = new mongoose.Schema({
	full_name: {
		type: String,
		trim: true,
		"default": "",
	},

	 email: {
        type: String,
        required: true,
        minlength: 10,
        trim: true,
       unique: true,
    },

	phone:{
		type: Number,
		"default": "",
		required: "Please fill valid phone number",
		unique: true,
		match : [/^[0]?[6789]\d{9}$/ , "Not Correct Format"]

	},
	
	immediate_access: {
		type:String,
		enum: ["yes", "no"],
		required:true,
		default: "no"
	},
	
	assets_info_nominee: {
		type: String,
		enum: ["Life Insurance", "Land Property", "Gold Property","others"],
		required:true,
		default: "Life Insurance"
	},
	
	reviews: {
		type: String,
		default: ""
	}

});

module.exports = mongoose.model('Nominee', NomineeSchema);
