const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const UserSchema = new mongoose.Schema({
    first_name:{
        type:String,
        required: true,
    },
    last_name:{
        type:String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        minlength: 10,
        trim: true,
       unique: true,
    },
    phone:{
        type: Number,
        required:true,
        minlength:10,
        unique:true,
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
    }
});

UserSchema.plugin(uniqueValidator);
UserSchema.pre('save', function(next) {
  let user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt
  .genSalt(12)
  .then((salt) => {
    return bcrypt.hash(user.password, salt);
  })
  .then((hash) => {
    user.password = hash;
    next();
  })
});

module.exports = mongoose.model('User', UserSchema);

