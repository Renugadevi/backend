const express = require('express');
const bcrypt = require('bcryptjs');
const Asset = require('../models/asset');
const router = express.Router();
const { check, validationResult } = require('express-validator');

router.post('/asset',async(req, res) => {
  try {
    const {country,organization,product,choose_file,nominee_access,expiry_date,comments} = req.body;
    const asset = new Asset({ country,organization,product,choose_file,nominee_access,expiry_date,comments});
    const persistedUser = await asset.save();
    res.status(201).json({
      title: 'asset insert Successful',
      detail: 'Successfully inserted new asset',
    });
  } catch (err) {
     res.status(400).json({
      errors: [
        {
          title: 'Insertion Error',
          detail: 'Something went wrong during insert process.',
          errorMessage: err.message,
        },
      ],
    });
  }
});
 //_id : 5 
router.get('/view-asset-details', function (req, res) {
    Asset.find({}, function (err, assets) {
        if (err) return res.status(500).send("No assets are stored.");
        res.status(200).send(assets);
    });
});
module.exports = router;
