const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');

const AssetSchema = new mongoose.Schema({
	country: {
		type: String,
		enum: ["India","US"],
		required:true,
		default: "India"
	},

	organization: {
		type: String,
		enum: ["Government Property", "Non-Government Property"],
		required:true,
		default: "Non-Government Property"
	},

	product: {
		type: String,
		enum: ["Life Insurance", "Land Property", "Gold Property","others"],
		required:true,
		default: "Land Property"
	},
	
	choose_file:{
		type: String,
		required : true,
	},
	nominee_access: {
		type:String,
		enum: ["yes", "no"],
		required:true,
		default: "no"
	},
	expiry_date: {
		type: Date,
		required:true,
		minlength:10,

	},
	comments: {
		type: String,
		default: ""
	}

});

AssetSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Asset', AssetSchema);

