const express = require('express');
const bcrypt = require('bcryptjs');
const Nominee = require('../models/nominee');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const isEmail = (email) => {
  if (typeof email !== 'string') {
    return false;
  }
  const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
  return emailRegex.test(email);
};
router.post('/nominee',async(req, res) => {
  try {
  
    const {full_name,email,phone,immediate_access,assets_info_nominee,reviews} = req.body;
    if (!isEmail(email)) {
      throw new Error('Email must be a valid email address.');
    }
    if (/^\d{10}$/.test(phone))
    {
      res.status(200);
    }
    else
    {
       throw new Error('invalid phone number');
    }
    const registereduser=await Nominee.findOne({ email});
    if(registereduser)
    {
      throw new Error('Nominee already exists');
    }
    const registerednumber=await Nominee.findOne({ phone});
    if(registerednumber)
    {
      throw new Error('Nominee already exists');
    }
    const nominee = new Nominee({ full_name,email,phone,immediate_access,assets_info_nominee,reviews});
    const persistedUser = await nominee.save();
    res.status(201).json({
      title: 'Nominee Registration Successful',
      detail: 'Successfully registered new nominee',
    });
  } catch (err) {
     res.status(400).json({
      errors: [
        {
          title: 'Insertion Error',
          detail: 'Something went wrong during insertion process.',
          errorMessage: err.message,
        },
      ],
    });
  }
});

router.get('/view-nominee-details', function (req, res) {
    Nominee.find({}, function (err, nominee) {
        if (err) return res.status(500).send("No nominee details are stored.");
        res.status(200).send(nominee);
    });
});
module.exports = router;
